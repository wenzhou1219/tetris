#include <Windows.h>
#include "Variable.h"

//颜色表
const COLORREF crColorTable[SQUARE_TYPENUM] = 
{
	RGB(100, 100, 100),			//边框
	RGB(255,102,0),		//I
	RGB(255,255,0),		//T
	RGB(255,0,0),			//O
	RGB(102,204,255),	//S
	RGB(0,255,0),			//Z
	RGB(0,0,255),			//L
	RGB(204,0,255)	//J
};

//小方块资源
const unsigned int uiIShape[DIRECTION_NUM][NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH] = 
{
	0,0,0,0,0,
	0,0,0,0,0,
	0,1,1,1,1,	
	0,0,0,0,0,
	0,0,0,0,0,

	0,0,1,0,0,
	0,0,1,0,0,
	0,0,1,0,0,
	0,0,1,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,0,0,0,0,
	0,1,1,1,1,
	0,0,0,0,0,
	0,0,0,0,0,

	0,0,1,0,0,
	0,0,1,0,0,
	0,0,1,0,0,
	0,0,1,0,0,
	0,0,0,0,0
};

const unsigned int uiTShape[DIRECTION_NUM][NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH] = 
{
	0,0,0,0,0,
	0,0,0,0,0,
	0,2,2,2,0,
	0,0,2,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,0,2,0,0,
	0,0,2,2,0,
	0,0,2,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,0,2,0,0,
	0,2,2,2,0,
	0,0,0,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,0,2,0,0,
	0,2,2,0,0,
	0,0,2,0,0,
	0,0,0,0,0
};

const unsigned int uiOShape[DIRECTION_NUM][NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH] = 
{
	0,0,0,0,0,
	0,0,3,3,0,
	0,0,3,3,0,
	0,0,0,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,0,3,3,0,
	0,0,3,3,0,
	0,0,0,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,0,3,3,0,
	0,0,3,3,0,
	0,0,0,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,0,3,3,0,
	0,0,3,3,0,
	0,0,0,0,0,
	0,0,0,0,0
};

const unsigned int uiSShape[DIRECTION_NUM][NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH] = 
{
	0,0,0,0,0,
	0,0,4,4,0,
	0,4,4,0,0,
	0,0,0,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,4,0,0,0,
	0,4,4,0,0,
	0,0,4,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,0,4,4,0,
	0,4,4,0,0,
	0,0,0,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,4,0,0,0,
	0,4,4,0,0,
	0,0,4,0,0,
	0,0,0,0,0
};

const unsigned int uiZShape[DIRECTION_NUM][NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH] = 
{
	0,0,0,0,0,
	0,5,5,0,0,
	0,0,5,5,0,
	0,0,0,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,0,5,0,0,
	0,5,5,0,0,
	0,5,0,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,5,5,0,0,
	0,0,5,5,0,
	0,0,0,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,0,5,0,0,
	0,5,5,0,0,
	0,5,0,0,0,
	0,0,0,0,0
};

const unsigned int uiLShape[DIRECTION_NUM][NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH] = 
{
	0,0,0,0,0,
	0,0,6,0,0,
	0,0,6,0,0,
	0,0,6,6,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,0,0,6,0,
	0,6,6,6,0,
	0,0,0,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,6,6,0,0,
	0,0,6,0,0,
	0,0,6,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,0,0,0,0,
	0,6,6,6,0,
	0,6,0,0,0,
	0,0,0,0,0
};

const unsigned int uiJShape[DIRECTION_NUM][NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH] = 
{
	0,0,0,0,0,
	0,0,7,0,0,
	0,0,7,0,0,
	0,7,7,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,0,0,0,0,
	0,7,7,7,0,
	0,0,0,7,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,0,7,7,0,
	0,0,7,0,0,
	0,0,7,0,0,
	0,0,0,0,0,

	0,0,0,0,0,
	0,7,0,0,0,
	0,7,7,7,0,
	0,0,0,0,0,
	0,0,0,0,0
};

//游戏工作窗口
unsigned int uiWorkWindow[WORK_WINDOW_HIGHT+1][WORK_WINDOW_WIDTH+2];

//下一个预览窗口
unsigned int	uiNextWindow[NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];

//等级、消除的行数、得分
unsigned int uiLevel , uiLines , uiScore ;

//下一个窗口显示的方块
	unsigned int uiNextShapeIndex;

//当前方块的位置、形状和方向
 int iCurrentXIndex, iCurrentYIndex;
 unsigned int uiCurrentShapeIndex,uiCurDirectionIndex;

 //当前方块向下运动速度
 BOOL bIsNormalSpeed;

 //游戏是否开始
 BOOL bISGameStart;

 //是否开启游戏声音
 BOOL bIsAudioOpen;