#include <Windows.h>
#include "Variable.h"
#include "OperateAndEstimate.h"
#include "DrawAndShow.h"

/************************************************************************/
/* 重置所有变量*/
/************************************************************************/
void ResetAll()
{
	int i,j;

	//游戏工作窗口
	for (i=0;i<WORK_WINDOW_HIGHT+1;i++)
	{
		for (j=0;j<WORK_WINDOW_WIDTH+2;j++)
		{
			if(i == WORK_WINDOW_HIGHT || 0==j || (WORK_WINDOW_WIDTH+1)==j)
			{
				uiWorkWindow[i][j] = 1;//最下面一行和左右两行做边缘
			}
			else 
			{
				uiWorkWindow[i][j] = 0;
			}
		}
	}

	//下一个预览窗口
	for (i=0;i<NEXT_WINDOW_HIGHT;i++)
	{
		for (j=0;j<NEXT_WINDOW_WIDTH;j++)
		{
			uiNextWindow[i][j] = 0;
		}
	}

	//等级、消除的行数、得分
	uiLevel =uiLines = uiScore=0 ;

	//产生下一个窗口显示的方块
	uiNextShapeIndex = GenerateNextShape();

	//当前方块的位置、形状和方向
	uiCurrentShapeIndex=uiNextShapeIndex;
	uiCurDirectionIndex=0;
	iCurrentXIndex=(WORK_WINDOW_WIDTH-NEXT_WINDOW_WIDTH)/2;
	iCurrentYIndex=GetStartPosY(uiCurrentShapeIndex);

	//再产生下一个窗口显示的方块
	uiNextShapeIndex = GenerateNextShape();

	//当前方块向下运动速度
	bIsNormalSpeed=TRUE;

	//游戏是否开始
	 bISGameStart=FALSE;

	 //游戏声效是否打开
	 bIsAudioOpen = TRUE;
}

/************************************************************************/
/* 改变当前显示的形状方向*/
/************************************************************************/
unsigned int GenerateNextShape()
{
	return (uiNextShapeIndex = ((int)rand())%7);
}

/************************************************************************/
/* 改变当前显示的形状方向*/
/************************************************************************/
void ChangeCurDirection()
{
	int i,j;
	unsigned int uiTempDirection;
	BOOL bFlag=TRUE;
	unsigned int (*uiTempShape)[NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];

	//获取当前翻转后的的方块
	 uiTempDirection = (uiCurDirectionIndex+1)%4;
	switch (uiCurrentShapeIndex)
	{
	case 0:
		uiTempShape = &uiIShape[uiTempDirection];
		break;
	case 1:
		uiTempShape = &uiTShape[uiTempDirection];
		break;
	case 2:
		uiTempShape =& uiOShape[uiTempDirection];
		break;
	case 3:
		uiTempShape = &uiSShape[uiTempDirection];
		break;
	case 4:
		uiTempShape = &uiZShape[uiTempDirection];
		break;
	case 5:
		uiTempShape = &uiLShape[uiTempDirection];
		break;
	case 6:
		uiTempShape = &uiJShape[uiTempDirection];
		break;
	}

	//判断是否还能翻转
	for (i=0;i<NEXT_WINDOW_HIGHT;i++)
	{
		for (j=0;j<NEXT_WINDOW_WIDTH;j++)
		{
			if ((*uiTempShape)[i][j]!=0 && ((i+iCurrentYIndex)<0 || (iCurrentXIndex+j+1)<0 ||uiWorkWindow[i+iCurrentYIndex][iCurrentXIndex+j+1]!=0))
			{
				bFlag = FALSE;
				break;
			}
		}

		if (FALSE == bFlag)
		{
			break;
		}
	}

	//还能够翻转
	if (TRUE == bFlag)
	{
			uiCurDirectionIndex = uiTempDirection;
			AddSound(TEXT(".\\sound\\turn.wav"));
	}
	else
	{
		MessageBeep(MB_ICONEXCLAMATION);
	}
}

/************************************************************************/
/* 当前形状向左移动*/
/************************************************************************/
BOOL LeftMove()
{
	int i,j;
	BOOL bFlag=TRUE;
	unsigned int (*uiTempShape)[NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];
	TCHAR szBuffer[256];

	//	//获取当前的方块
	switch (uiCurrentShapeIndex)
	{
	case 0:
		uiTempShape = &uiIShape[uiCurDirectionIndex];
		break;
	case 1:
		uiTempShape = &uiTShape[uiCurDirectionIndex];
		break;
	case 2:
		uiTempShape =& uiOShape[uiCurDirectionIndex];
		break;
	case 3:
		uiTempShape = &uiSShape[uiCurDirectionIndex];
		break;
	case 4:
		uiTempShape = &uiZShape[uiCurDirectionIndex];
		break;
	case 5:
		uiTempShape = &uiLShape[uiCurDirectionIndex];
		break;
	case 6:
		uiTempShape = &uiJShape[uiCurDirectionIndex];
		break;
	}

	//判断是否还能左移
	for (i=0;i<NEXT_WINDOW_HIGHT;i++)
	{
		for (j=0;j<NEXT_WINDOW_WIDTH;j++)
		{
			if ((*uiTempShape)[i][j]!=0 && uiWorkWindow[i+iCurrentYIndex][iCurrentXIndex+j]!=0)
			{
				//wsprintf(szBuffer,TEXT("%u,%d,%d,%d"),(*uiTempShape)[i][j],uiWorkWindow[i][iCurrentXIndex+j],j,iCurrentXIndex);
				//MessageBox(NULL,szBuffer,TEXT("sdas"),MB_OK);

				bFlag = FALSE;
				break;
			}
		}

		if (FALSE == bFlag)
		{			
			break;
		}
	}

	//还能够左移
	if (TRUE == bFlag)
	{
			iCurrentXIndex -=1;
			AddSound(TEXT(".\\sound\\move.wav"));
	}
	else
	{
		MessageBeep(MB_ICONEXCLAMATION);
	}

	return bFlag;
}

/************************************************************************/
/* 当前形状向右移动*/
/************************************************************************/
BOOL RightMove()
{
	int i,j;
	BOOL bFlag=TRUE;
	unsigned int (*uiTempShape)[NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];

	//	//获取当前的方块
	switch (uiCurrentShapeIndex)
	{
	case 0:
		uiTempShape = &uiIShape[uiCurDirectionIndex];
		break;
	case 1:
		uiTempShape = &uiTShape[uiCurDirectionIndex];
		break;
	case 2:
		uiTempShape =& uiOShape[uiCurDirectionIndex];
		break;
	case 3:
		uiTempShape = &uiSShape[uiCurDirectionIndex];
		break;
	case 4:
		uiTempShape = &uiZShape[uiCurDirectionIndex];
		break;
	case 5:
		uiTempShape = &uiLShape[uiCurDirectionIndex];
		break;
	case 6:
		uiTempShape = &uiJShape[uiCurDirectionIndex];
		break;
	}

	//判断是否还能右移
	for (i=0;i<NEXT_WINDOW_HIGHT;i++)
	{
		for (j=0;j<NEXT_WINDOW_WIDTH;j++)
		{
			if ((*uiTempShape)[i][j]!=0 && uiWorkWindow[i+iCurrentYIndex][iCurrentXIndex+j+2]!=0)
			{
				bFlag = FALSE;
				break;
			}
		}

		if (FALSE == bFlag)
		{
			break;
		}
	}

	//还能够右移
	if (TRUE == bFlag)
	{
		iCurrentXIndex +=1;
		AddSound(TEXT(".\\sound\\move.wav"));
	}
	else
	{
		MessageBeep(MB_ICONEXCLAMATION);
	}

	return bFlag;
}

/************************************************************************/
/* 当前形状向下移动*/
/************************************************************************/
BOOL DownMove()
{
	int i,j;
	BOOL bFlag=TRUE;
	unsigned int (*uiTempShape)[NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];

	//	//获取当前的方块
	switch (uiCurrentShapeIndex)
	{
	case 0:
		uiTempShape = &uiIShape[uiCurDirectionIndex];
		break;
	case 1:
		uiTempShape = &uiTShape[uiCurDirectionIndex];
		break;
	case 2:
		uiTempShape =& uiOShape[uiCurDirectionIndex];
		break;
	case 3:
		uiTempShape = &uiSShape[uiCurDirectionIndex];
		break;
	case 4:
		uiTempShape = &uiZShape[uiCurDirectionIndex];
		break;
	case 5:
		uiTempShape = &uiLShape[uiCurDirectionIndex];
		break;
	case 6:
		uiTempShape = &uiJShape[uiCurDirectionIndex];
		break;
	}

	//判断是否还能下移
	for (i=0;i<NEXT_WINDOW_HIGHT;i++)
	{
		for (j=0;j<NEXT_WINDOW_WIDTH;j++)
		{
			if ((*uiTempShape)[i][j]!=0 && uiWorkWindow[iCurrentYIndex+1+i][j+1+iCurrentXIndex]!=0)
			{
				bFlag = FALSE;
				break;
			}
		}

		if (FALSE == bFlag)
		{
			break;
		}
	}

	//还能够下移
	if (TRUE == bFlag)
	{
		iCurrentYIndex +=1;
	}

	return bFlag;
}

/************************************************************************/
/* 改变向下移动速度*/
/************************************************************************/
void SetMoveSpeed(HWND hwnd, BOOL bIsSetNormalSpeed)
{
	if (bIsNormalSpeed != bIsSetNormalSpeed)
	{
		if(TRUE == bIsSetNormalSpeed)
		{
			SetTimer(hwnd, TIMER_1,NORMAL_TIME_ELAPSE,NULL);
			bIsNormalSpeed = TRUE;
		}
		else
		{
			SetTimer(hwnd, TIMER_1,QUICK_TIME_ELAPSE,NULL);
			bIsNormalSpeed = FALSE;
		}
	}
}

/************************************************************************/
/* 判断当前工作区小方块是否能够消行，如果能消行则消去，返回消去的行数*/
/************************************************************************/
unsigned int EliminateSquares()
{
	int i,j,m,n;
	BOOL bIsEliminate=TRUE;
	unsigned int uiEliminateLines=0;

	for (i=WORK_WINDOW_HIGHT-1;i>=0;i--)
	{
		for(j=1;j<WORK_WINDOW_WIDTH+1;j++)
		{
			if (uiWorkWindow[i][j]==0)
			{
				bIsEliminate=FALSE;//只要一行中找到一个0方块就不能消去
				break;
			}
		}

		//能够消除则依次把上面的行移下来
		if (TRUE == bIsEliminate)
		{
			for (m=i;m>0;m--)
			{
				for(j=1;j<WORK_WINDOW_WIDTH+1;j++)
				{
					uiWorkWindow[m][j]=uiWorkWindow[m-1][j];
				}
			}

			uiEliminateLines+=1;//消去行数加1
			i+=1;//由于上面的行落到下面所以必须对当前行再做检查
		}
		else
		{
			bIsEliminate = TRUE;//重新置为TRUE以便下一次检查
		}
	}

	return uiEliminateLines;
}

/************************************************************************/
/* 根据当前消去的行数计算得分，更新当前分数和行数信息*/
/************************************************************************/
void CalcState(unsigned int uiEliminateLines)
{
	uiLines += uiEliminateLines;//当前消去行数
	uiScore += uiEliminateLines*uiEliminateLines;//当前得分
}

/************************************************************************/
/* 根据当前输入的形状来判断最开始出现的位置保证方块的最上面和工作区最上面一行对齐*/
/************************************************************************/
int GetStartPosY(unsigned int uiShapeIndex)
{
	switch(uiShapeIndex)
	{
	case 0:
		return -2;
	case 1:
		return -2;
	case 2:
		return -1;
	case 3:
		return -1;
	case 4:
		return -1;
	case 5:
		return -1;
	case 6:
		return -1;
	}
}

/************************************************************************/
/* 判断游戏是否结束*/
/************************************************************************/
BOOL IsGameOver()
{
	int j;
	BOOL bFlag=FALSE;

	for (j=1;j<WORK_WINDOW_WIDTH+1;j++)
	{
		if (uiWorkWindow[0][j]!=0)
		{
			bFlag = TRUE;
		}
	}

	return bFlag;
}

/************************************************************************/
/* 给游戏加上音效*/
/************************************************************************/
void AddSound(PCSTR szSoundName)
{
	if (TRUE == bIsAudioOpen)
	{
		PlaySound(szSoundName, NULL, SND_FILENAME | SND_ASYNC);
	}
}
