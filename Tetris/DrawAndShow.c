#include <Windows.h>
#include "Variable.h"

/************************************************************************/
/* 功能:绘制一个指定颜色和大小正方块*/
/************************************************************************/
void DrawSquare(const HDC hdc, const  unsigned int xPos, const  unsigned int yPos, const unsigned int uiColorIndex)
{
	HBRUSH hBrush, hOldBrush;

	hBrush = (HBRUSH)CreateSolidBrush(crColorTable[uiColorIndex]);
	hOldBrush = SelectObject(hdc, hBrush);
	Rectangle(hdc, xPos, yPos, xPos+SQUARE_SIZE, yPos+SQUARE_SIZE);
	SelectObject(hdc, hOldBrush);
	DeleteObject(hBrush);
}

/************************************************************************/
/* 在指定位置的一个矩形区域内绘制若干正方块，在什么位置绘制由输入指定*/
/************************************************************************/
void DrawSquaresArray(const HDC hdc, const  unsigned int xPos, const  unsigned int yPos, 
											unsigned int uiSquareArray[NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH])
{
	unsigned int i,j;

	for (i = 0; i < NEXT_WINDOW_HIGHT; i++)
	{
		for (j = 0; j < NEXT_WINDOW_WIDTH; j++)
		{
			if (0 != uiSquareArray[i][j])
			{
				DrawSquare(hdc, xPos + j*SQUARE_SIZE-j, yPos+i*SQUARE_SIZE-i, uiSquareArray[i][j]);
			}
		}
	}
}

/************************************************************************/
/* 由指定输入在下一个窗口绘制下一个图形*/
/************************************************************************/
void DrawNextWindow(const HDC hdc, unsigned int uiShapeIndex)
{
	unsigned int (*uiTempShape)[NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];

	//根据不同输入选择显示的图形
	switch (uiShapeIndex)
	{
	case 0:
		uiTempShape = &uiIShape[0];
		break;
	case 1:
		uiTempShape = &uiTShape[0];
		break;
	case 2:
		uiTempShape =& uiOShape[0];
		break;
	case 3:
		uiTempShape = &uiSShape[0];
		break;
	case 4:
		uiTempShape = &uiZShape[0];
		break;
	case 5:
		uiTempShape = &uiLShape[0];
		break;
	case 6:
		uiTempShape = &uiJShape[0];
		break;
	}

	//清除上次绘制的图形
// 	SaveDC(hdc);
// 	SelectObject(hdc, (HBRUSH) GetStockObject (WHITE_BRUSH));
// 
// 	RestoreDC(hdc,-1);

	//绘制外部边缘和清除上次绘制的图形
	SaveDC(hdc);
	SelectObject(hdc, (HBRUSH)GetStockObject(WHITE_BRUSH));
	SelectObject(hdc,(HPEN)CreatePen(PS_DASHDOTDOT,MYWINDOW_BORDER,MYWINDOW_BORDER_COLOR));
	Rectangle(hdc,
		2*MYWINDOW_SPACE+2*MYWINDOW_BORDER+WORK_WINDOW_WIDTH*SQUARE_SIZE-(WORK_WINDOW_WIDTH-1),
		MYWINDOW_SPACE,
		2*MYWINDOW_SPACE+3*MYWINDOW_BORDER+WORK_WINDOW_WIDTH*SQUARE_SIZE-(WORK_WINDOW_WIDTH-1)+NEXT_WINDOW_WIDTH*SQUARE_SIZE-(NEXT_WINDOW_WIDTH-1)+MYWINDOW_BORDER,
		MYWINDOW_SPACE+MYWINDOW_BORDER+NEXT_WINDOW_HIGHT*SQUARE_SIZE-(NEXT_WINDOW_HIGHT-1)+MYWINDOW_BORDER);
	RestoreDC(hdc,-1);

	//绘制新的图形
	DrawSquaresArray(hdc,
		2*MYWINDOW_SPACE+3*MYWINDOW_BORDER+WORK_WINDOW_WIDTH*SQUARE_SIZE-(WORK_WINDOW_WIDTH-1),
		MYWINDOW_SPACE+MYWINDOW_BORDER,
		*uiTempShape);
}

/************************************************************************/
/* 绘制当前正在移动的图形*/
/************************************************************************/
void DrawCurrentSquare(const HDC hdc,  int xIndex,  int yIndex,unsigned int uiCurrentShapeIndex,unsigned int uiCurDirectionIndex)
{
	unsigned int (*uiTempShape)[NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];

	//根据不同输入选择显示的图形和方向
	switch (uiCurrentShapeIndex)
	{
	case 0:
		uiTempShape = &uiIShape[uiCurDirectionIndex];
		break;
	case 1:
		uiTempShape = &uiTShape[uiCurDirectionIndex];
		break;
	case 2:
		uiTempShape =& uiOShape[uiCurDirectionIndex];
		break;
	case 3:
		uiTempShape = &uiSShape[uiCurDirectionIndex];
		break;
	case 4:
		uiTempShape = &uiZShape[uiCurDirectionIndex];
		break;
	case 5:
		uiTempShape = &uiLShape[uiCurDirectionIndex];
		break;
	case 6:
		uiTempShape = &uiJShape[uiCurDirectionIndex];
		break;
	}

	DrawSquaresArray(hdc,
		MYWINDOW_SPACE+MYWINDOW_BORDER + xIndex*SQUARE_SIZE-xIndex, 
		MYWINDOW_SPACE+MYWINDOW_BORDER+ yIndex*SQUARE_SIZE-yIndex,
		*uiTempShape);
}

/************************************************************************/
/* 在指定位置的一个矩形区域内绘制当前工作图，在什么位置绘制由输入指定*/
/************************************************************************/
void DrawWorkWindow(const HDC hdc)
{
	unsigned int i,j;

	//绘制内部内容
	for (i = 0; i < WORK_WINDOW_HIGHT; i++)
	{
		for (j = 0; j < WORK_WINDOW_WIDTH; j++)
		{
			//if (0 != uiWorkWindow[i][j])
			{
				DrawSquare(hdc, MYWINDOW_SPACE+MYWINDOW_BORDER + j*SQUARE_SIZE-j, 
												MYWINDOW_SPACE+MYWINDOW_BORDER+i*SQUARE_SIZE-i, uiWorkWindow[i][j+1]);
			}
		}
	 }

	//绘制外部边缘
	SaveDC(hdc);
	SelectObject(hdc, (HBRUSH)GetStockObject(NULL_BRUSH));
	SelectObject(hdc,(HPEN)CreatePen(PS_DASHDOTDOT,MYWINDOW_BORDER,MYWINDOW_BORDER_COLOR));
	Rectangle(hdc,MYWINDOW_SPACE,MYWINDOW_SPACE,
							  MYWINDOW_SPACE+MYWINDOW_BORDER+WORK_WINDOW_WIDTH*SQUARE_SIZE-(WORK_WINDOW_WIDTH-1)+MYWINDOW_BORDER,
							  MYWINDOW_SPACE+MYWINDOW_BORDER+WORK_WINDOW_HIGHT*SQUARE_SIZE-(WORK_WINDOW_HIGHT-1)+MYWINDOW_BORDER);
	RestoreDC(hdc,-1);
}

/************************************************************************/
/* 在指定位置的一个矩形区域内绘制当前的等级、消去行数和得分*/
/************************************************************************/
void ShowCurrentState(const HDC hdc)
{
	TCHAR szBuffer[256];
	TEXTMETRIC tm;
	int textHeight;

	//获得字体信息
	GetTextMetrics(hdc,&tm);
	textHeight=tm.tmHeight;

	//绘制外部边缘
	SaveDC(hdc);
	SelectObject(hdc, (HBRUSH)GetStockObject(WHITE_BRUSH));
	SelectObject(hdc,(HPEN)CreatePen(PS_DASHDOTDOT,MYWINDOW_BORDER,MYWINDOW_BORDER_COLOR));
	Rectangle(hdc,
		2*MYWINDOW_SPACE+2*MYWINDOW_BORDER+WORK_WINDOW_WIDTH*SQUARE_SIZE-(WORK_WINDOW_WIDTH-1),
		MYWINDOW_SPACE+MYWINDOW_BORDER+NEXT_WINDOW_HIGHT*SQUARE_SIZE-(NEXT_WINDOW_HIGHT-1)+MYWINDOW_BORDER+MYWINDOW_SPACE,
		2*MYWINDOW_SPACE+3*MYWINDOW_BORDER+WORK_WINDOW_WIDTH*SQUARE_SIZE-(WORK_WINDOW_WIDTH-1)+NEXT_WINDOW_WIDTH*SQUARE_SIZE-(NEXT_WINDOW_WIDTH-1)+MYWINDOW_BORDER,
		MYWINDOW_SPACE+MYWINDOW_BORDER+NEXT_WINDOW_HIGHT*SQUARE_SIZE-(NEXT_WINDOW_HIGHT-1)+MYWINDOW_BORDER+MYWINDOW_SPACE+3*MYWINDOW_SPACE+4*textHeight
		);
	RestoreDC(hdc,-1);

	//显示当前等级、消去行数和得分
	wsprintf(szBuffer, TEXT("等级:%d"),uiLevel);
	TextOut(hdc,
		3*MYWINDOW_SPACE+3*MYWINDOW_BORDER+WORK_WINDOW_WIDTH*SQUARE_SIZE-(WORK_WINDOW_WIDTH-1),
		MYWINDOW_SPACE+MYWINDOW_BORDER+NEXT_WINDOW_HIGHT*SQUARE_SIZE-(NEXT_WINDOW_HIGHT-1)+MYWINDOW_BORDER+MYWINDOW_SPACE+MYWINDOW_SPACE+MYWINDOW_BORDER,
		szBuffer,
		lstrlen(szBuffer)
		);
	wsprintf(szBuffer, TEXT("行数:%d"),uiLines);
	TextOut(hdc,
		3*MYWINDOW_SPACE+3*MYWINDOW_BORDER+WORK_WINDOW_WIDTH*SQUARE_SIZE-(WORK_WINDOW_WIDTH-1),
		MYWINDOW_SPACE+MYWINDOW_BORDER+NEXT_WINDOW_HIGHT*SQUARE_SIZE-(NEXT_WINDOW_HIGHT-1)+MYWINDOW_BORDER+MYWINDOW_SPACE+MYWINDOW_SPACE*2+MYWINDOW_BORDER+textHeight,
		szBuffer,
		lstrlen(szBuffer)
		);
	wsprintf(szBuffer, TEXT("得分:%d"),uiScore);
	TextOut(hdc,
		3*MYWINDOW_SPACE+3*MYWINDOW_BORDER+WORK_WINDOW_WIDTH*SQUARE_SIZE-(WORK_WINDOW_WIDTH-1),
		MYWINDOW_SPACE+MYWINDOW_BORDER+NEXT_WINDOW_HIGHT*SQUARE_SIZE-(NEXT_WINDOW_HIGHT-1)+MYWINDOW_BORDER+MYWINDOW_SPACE+MYWINDOW_SPACE*3+MYWINDOW_BORDER+2*textHeight,
		szBuffer,
		lstrlen(szBuffer)
		);
}

/************************************************************************/
/* 动态刷新工作窗口,显示工作窗口和当前的块*/
/************************************************************************/
void RefreshWorkWindow(HWND hwnd)
{
	HDC hdc;
	HRGN hRgnClip;

	//创建剪切区域避免方块绘制超出范围
	hRgnClip = CreateRectRgn(MYWINDOW_SPACE+MYWINDOW_BORDER,MYWINDOW_SPACE+MYWINDOW_BORDER,
		MYWINDOW_SPACE+MYWINDOW_BORDER+WORK_WINDOW_WIDTH*SQUARE_SIZE-(WORK_WINDOW_WIDTH-1),
		MYWINDOW_SPACE+MYWINDOW_BORDER+WORK_WINDOW_HIGHT*SQUARE_SIZE-(WORK_WINDOW_HIGHT-1));

	hdc = GetDC(hwnd);
	DrawWorkWindow(hdc);

	SelectClipRgn(hdc,hRgnClip);
	DrawCurrentSquare(hdc,iCurrentXIndex,iCurrentYIndex,uiCurrentShapeIndex,uiCurDirectionIndex);
	DeleteObject(hRgnClip);

	ReleaseDC(hwnd,hdc);
	UpdateWindow(hwnd);
}