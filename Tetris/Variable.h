#ifndef VARIABLE_H_H_H
#define VARIABLE_H_H_H

#include <Windows.h>

#define MYWINDOW_BORDER 5
#define MYWINDOW_BORDER_COLOR (RGB(100,100,255))
#define MYWINDOW_SPACE 20
#define SQUARE_SIZE 30
#define SQUARE_TYPENUM 8
#define DIRECTION_NUM	4
#define NEXT_WINDOW_WIDTH 5
#define NEXT_WINDOW_HIGHT 5
#define WORK_WINDOW_WIDTH 10
#define WORK_WINDOW_HIGHT 20
#define BUTTON_WIDTH 80
#define BUTTON_HIGHT 20

#define TIMER_1 1
#define NORMAL_TIME_ELAPSE 1000
#define QUICK_TIME_ELAPSE 200

extern  const COLORREF crColorTable[SQUARE_TYPENUM];
extern  const unsigned int uiIShape[DIRECTION_NUM][NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];
extern  const unsigned int uiTShape[DIRECTION_NUM][NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];
extern  const unsigned int uiOShape[DIRECTION_NUM][NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];
extern  const unsigned int uiSShape[DIRECTION_NUM][NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];
extern  const unsigned int uiZShape[DIRECTION_NUM][NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];
extern  const unsigned int uiLShape[DIRECTION_NUM][NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];
extern  const unsigned int uiJShape[DIRECTION_NUM][NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];
extern  unsigned int  uiWorkWindow[WORK_WINDOW_HIGHT+1][WORK_WINDOW_WIDTH+2];
extern  unsigned int	uiNextWindow[NEXT_WINDOW_HIGHT][NEXT_WINDOW_WIDTH];
extern  unsigned int uiLevel;
extern  unsigned int uiLines;
extern  unsigned int uiScore;
extern  unsigned int uiNextShapeIndex;
extern  int iCurrentXIndex;
extern  int iCurrentYIndex;
 extern unsigned int uiCurrentShapeIndex;
 extern unsigned int  uiCurDirectionIndex;
 extern BOOL bIsNormalSpeed;
 extern  BOOL bISGameStart;
 extern  BOOL bIsAudioOpen;
#endif